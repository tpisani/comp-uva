#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>


typedef struct symbol {
    int id;
    char name;
    struct symbol *next;
} symbol;


symbol* Symbol() {
    return (symbol *) malloc(sizeof(symbol));
}


typedef struct {
    symbol *head, *tail;
} symbol_table;


symbol_table* SymbolTable() {
    return (symbol_table *) malloc(sizeof(symbol_table));
}


int is_empty(symbol_table *table) {
    if(table->head == NULL) {
        return 1;
    }
    return 0;
}


void free_table(symbol_table *table) {
    symbol *head = table->head, *curr;
    while(head != NULL) {
        curr = head;
        head = head->next;
        free(curr);
    }
    free(table);
}


symbol* insert_symbol(symbol_table *table, char name) {
    symbol *new_symbol = Symbol();
    new_symbol->name = name;
    new_symbol->next = NULL;
    if(is_empty(table)) {
        new_symbol->id = 1;
        table->head = new_symbol;
        table->tail = new_symbol;
    } else {
        new_symbol->id = table->tail->id + 1;
        table->tail->next = new_symbol;
        table->tail = new_symbol;
    }
    return new_symbol;
}


symbol* lookup_symbol(symbol_table *table, char name) {
    symbol *curr = table->head;
    while(curr != NULL) {
        if(curr->name == name) {
            return curr;
        }
        curr = curr->next;
    }
    return NULL;
}


symbol* lookup_insert_symbol(symbol_table *table, char name) {
    symbol *sym = lookup_symbol(table, name);
    if(sym == NULL) {
        sym = insert_symbol(table, name);
    }
    return sym;
}


char* String(int size) {
    return (char *) malloc(sizeof(char) * size);
}


void remove_spaces(char *string) {
    char *i = string;
    char *j = string;
    while(*j != 0) {
        *i = *j++;
        if(*i != ' ') {
            i++;
        }
    }
    *i = 0;
}


char* concat_argv(int argc, char **argv) {
    int i, size = 1;
    char *string;
    for(i = 1; i < argc; i++) {
        size += strlen(argv[i]) + 1;
    }
    string = String(size);
    strcpy(string, argv[1]);
    for(i = 2; i < argc; i++) {
        strcat(string, " ");
        strcat(string, argv[i]);
    }
    return string;
}


int is_var(char c) {
    switch(c) {
        case '=':
        case '+':
        case '-':
        case '*':
        case '/':
        case '(':
        case ')':
            return 0;
            break;
        default:
            if(isdigit(c)) {
                return 0;
            }
            return 1;
            break;
    }
}


char* tokenize(char *string) {
    // <X> = 3 characters.
    // <id,X> = 6 characters.
    int i, target = 0, size = 9, const_size = 3, token_size = 6;
    char *tokenized = String(size), curr;
    symbol_table *table = SymbolTable();
    remove_spaces(string);
    for(i = 0; i < strlen(string); i++) {
        curr = string[i];
        if(is_var(curr)) {
            size += token_size;
            symbol *sym = lookup_insert_symbol(table, curr);
            target += sprintf(tokenized + target, "<id,%i>", sym->id);
        } else {
            size += const_size;
            target += sprintf(tokenized + target, "<%c>", curr);
        }
        tokenized = (char *) realloc(tokenized, size);
    }
    free_table(table);
    return tokenized;
}


int main(int argc, char **argv) {
    if(argc <= 1) {
        printf("Usage: %s <expr>\n", argv[0]);
    } else {
        char *input, *tokenized;
        input = concat_argv(argc, argv);
        tokenized = tokenize(input);
        printf("%s\n", tokenized);
        free(input);
        free(tokenized);
    }
    return 0;
}
