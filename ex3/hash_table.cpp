#include <iostream>
#include <cstring>

using namespace std;


unsigned int genHash(char *s) {
    unsigned int hval = 0;
    while(*s != 0) {
        hval = (hval << 4) + *s;
        s++;
    }
    return hval;
}


class Symbol {

    friend class HashTable;

    private:
        unsigned int symId;
        char *name;
        Symbol *list;
        Symbol *next;

    public:
        Symbol(char *);
        ~Symbol();
        unsigned int id();
        Symbol* nextInBucket();
        Symbol* nextInList();

};


Symbol::Symbol(char *s) {
    name = new char[strlen(s) + 1];
    strcpy(name, s);
    next = NULL;
    list = NULL;
}


Symbol::~Symbol() {
    delete[] name;
    name = NULL;
    next = NULL;
    list = NULL;
}


unsigned int Symbol::id() {
    return symId;
}


Symbol* Symbol::nextInBucket() {
    return next;
}


Symbol* Symbol::nextInList() {
    return list;
}


const unsigned int HT_SIZE = 7;

class HashTable {

    private:
        Symbol *HT[HT_SIZE];
        Symbol *first;
        Symbol *last;
        unsigned int count;

    public:
        HashTable();
        ~HashTable();
        void reset();
        Symbol* clear();
        Symbol* lookup(char *);
        Symbol* lookup(char *, unsigned int);
        Symbol* insert(char *, unsigned int);
        Symbol* lookupInsert(char *);
        Symbol* symbolList();
        unsigned int symbolCount();
        void print();
        void printSymbol(Symbol*);
        void printSymbolBucket(Symbol*);

};


void HashTable::reset() {
    unsigned int i = 0;
    for(i = 0; i < HT_SIZE; i++) {
        HT[i] = NULL;
    }
    first = NULL;
    last = NULL;
    count = 0;
}


HashTable::HashTable() {
    reset();
}


HashTable::~HashTable() {
    reset();
}


Symbol* HashTable::clear() {
    Symbol *list = first;
    reset();
    return list;
}


Symbol* HashTable::lookup(char *s) {
    unsigned int h = genHash(s);
    return lookup(s, h);
}


Symbol* HashTable::lookup(char *s, unsigned int h) {
    unsigned int index = h % HT_SIZE;
    Symbol *sym = HT[index];
    while(sym != NULL) {
        if(strcmp(sym->name, s) == 0) {
            break;
        }
        sym = sym->next;
    }
    return sym;
}


Symbol* HashTable::insert(char *s, unsigned int h) {
    unsigned int index = h % HT_SIZE;
    Symbol *sym = new Symbol(s);
    sym->next = HT[index];
    HT[index] = sym;
    if(count == 0) {
        sym->symId = 1;
        first = last = sym;
    } else {
        sym->symId = last->symId + 1;
        last->list = sym;
        last = sym;
    }
    count++;
    return sym;
}


Symbol* HashTable::lookupInsert(char *s) {
    unsigned int h = genHash(s);
    Symbol *sym;
    sym = lookup(s, h);
    if(sym == NULL) {
        sym = insert(s, h);
    }
    return sym;
}


Symbol* HashTable::symbolList() {
    return first;
}


unsigned int HashTable::symbolCount() {
    return count;
}


void HashTable::printSymbol(Symbol *sym) {
    cout << " [";
    cout << sym->id() << ": ";
    cout << sym->name;
    cout << " (";
    cout << "list: ";
    if(sym->list != NULL) {
        cout << sym->list->id();
    } else {
        cout << "NULL";
    }
    cout << ", next: ";
    if(sym->next != NULL) {
        cout << sym->next->id();
    } else {
        cout << "NULL";
    }
    cout << ")";
    cout << "]";
}


void HashTable::printSymbolBucket(Symbol *bucket) {
    Symbol *sym = bucket;
    while(sym != NULL) {
        printSymbol(sym);
        sym = sym->next;
    }
}


void HashTable::print() {
    unsigned int i;
    for(i = 0; i < HT_SIZE; i++) {
        cout << i << ":";
        if(HT[i] == NULL) {
            cout << " NULL";
        } else {
            printSymbolBucket(HT[i]);
        }
        cout << endl;
    }
}


bool is_var(char *s) {
    char c = s[0];
    switch(c) {
        case '+':
        case '-':
        case '/':
        case '*':
        case '=':
            return false;
            break;
        default:
            if(isdigit(c)) {
                return false;
            }
            return true;
    }
}


int main(int argc, char *argv[]) {
    HashTable *table = new HashTable();
    for(int i = 1; i < argc; i++) {
        char *lexem = argv[i];
        if(is_var(lexem)) {
            table->lookupInsert(lexem);
        }
    }
    table->print();
    delete table;
    return 0;
}
