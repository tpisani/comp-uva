from token import *

__all__ = ["Lexer"]


class Lexer(object):

    def __init__(self, stream=""):
        self.clear()
        self.input(stream)

    def _reset(self):
        self.lexpos = 0
        self.lineno = 1
        self.column = 1

    def _walk(self, keep):
        endpos = self.lexpos
        while endpos < self.stream_len:
            current = self.stream[endpos]
            if keep(current):
                endpos += 1
                self.column += 1
            else:
                break
        lexeme = self.stream[self.lexpos:endpos]
        self.lexpos = endpos
        return lexeme

    def _process_identifier(self):
        lexeme = self._walk(lambda c: c.isalnum())
        return IdentifierToken(lexeme)

    def _process_number(self):
        lexeme = self._walk(lambda c: c.isdigit())
        return NumberToken(lexeme)

    def _process_operator(self):
        lexeme = self.stream[self.lexpos]
        operator_type = OPERATORS_DICT[lexeme]
        token = operator_type(lexeme)
        self.lexpos += 1
        self.column += 1
        return token

    def clear(self):
        self._reset()
        self.stream = ""
        self.stream_len = 0

    def input(self, stream):
        self.stream += stream
        self.stream_len += len(stream)

    def tokens(self):
        while self.lexpos < self.stream_len:
            current = self.stream[self.lexpos]

            if current in IGNORE:
                self.lexpos += 1
                if current == EOL:
                    self.lineno += 1
                    self.column = 0
                else:
                    self.column += 1
                continue

            if current.isalpha():
                yield self._process_identifier()
            elif current.isdigit():
                yield self._process_number()
            elif current in OPERATORS:
                yield self._process_operator()
            else:
                raise UnrecognizedTokenError(self.lineno, self.column)

        self._reset()

    def validate(self):
        for token in self.tokens():
            pass
