
EOL = "\n"

WHITESPACE = " "

ASSIGNMENT = "="

MINUS = "-"

ADD = "+"

DIVIDE = "/"

TIMES = "*"

OPERATORS = (ASSIGNMENT,
             MINUS,
             ADD,
             DIVIDE,
             TIMES)

IGNORE = (WHITESPACE, EOL)


class UnrecognizedTokenError(Exception):

    def __init__(self, lineno, column):
        self.lineno = lineno
        self.column = column

    def __str__(self):
        return "Unrecognized token at line {e.lineno}:{e.column}".format(e=self)


class Token(object):

    def __str__(self):
        return "<{classname}: {str}>".format(classname=self.__class__.__name__,
                                             str=self.string())

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        if isinstance(other, self.__class__) and self.string() == other.string():
            return True
        return False

    def string(self):
        raise NotImplementedError


class IdentifierToken(Token):

    def __init__(self, name):
        self.name = name

    def string(self):
        return self.name


class NumberToken(Token):

    def __init__(self, value):
        self.value = int(value)

    def string(self):
        return str(self.value)


class OperatorToken(Token):

    def __init__(self, op):
        self.op = op

    def string(self):
        return self.op


class AssignmentOperator(OperatorToken):
    pass


class MinusOperator(OperatorToken):
    pass


class AddOperator(OperatorToken):
    pass


class DivideOperator(OperatorToken):
    pass


class TimesOperator(OperatorToken):
    pass


OPERATORS_DICT = {
    ASSIGNMENT: AssignmentOperator,
    MINUS: MinusOperator,
    ADD: AddOperator,
    DIVIDE: DivideOperator,
    TIMES: TimesOperator,
}
