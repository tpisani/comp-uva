from lexer import Lexer
from lexer.token import *

from symbol import SymbolTable

from tree import ExpressionTree, ExpressionNode


def lines(stream):
    stream_lines = stream.splitlines()
    return filter(None, stream_lines)


class Parser(object):
    """
    Grammar rules
    stmt ::= ID = expr
    expr ::= expr + term
        | expr - term
        | term
    term ::= term * factor
        | term / factor
        | factor
    factor ::= ID
        | NUM
    """

    def _advance(self):
        self.token, self.next_token = self.next_token, next(self.tokens, None)

    def _accept(self, token_type):
        if self.next_token and isinstance(self.next_token, token_type):
            self._advance()
            return True
        return False

    def _stmt(self):
        stmtval = self._expr()
        while self._accept(AssignmentOperator):
            operator = self.token
            right = self._expr()
            stmtval = self.tree.add_node(operator.op, stmtval, right)
        return stmtval

    def _expr(self):
        exprval = self._term()
        while self._accept((AddOperator, MinusOperator)):
            operator = self.token
            right = self._term()
            exprval = self.tree.add_node(operator.op, exprval, right)
        return exprval

    def _term(self):
        termval = self._factor()
        while self._accept((TimesOperator, DivideOperator)):
            operator = self.token
            right = self._factor()
            termval = self.tree.add_node(operator.op, termval, right)
        return termval

    def _factor(self):
        if self._accept((IdentifierToken, NumberToken)):
            if isinstance(self.token, IdentifierToken):
                return self.sym_table.lookup_insert(self.token.name)
            return self.token.value
        raise SyntaxError("Unexpected token {t}".format(t=self.token.string()))

    def parse(self, stream):
        lineno = 0
        self.sym_table = SymbolTable()
        for line in lines(stream):
            lineno += 1
            lexer = Lexer(line)
            lexer.lineno = lineno
            self.tokens = lexer.tokens()
            self.token = None
            self.next_token = None
            self.tree = ExpressionTree()
            self._advance()
            self._stmt()
            yield self.tree


class PostfixParser(object):
    """
    Grammar rules
    stmt ::= ID = expr
    expr ::= expr factor +
        | expr factor -
        | expr factor *
        | expr factor /
        | factor
    factor ::= ID
        | NUM
    """

    def _value(self, token):
        if isinstance(token, IdentifierToken):
            value = self.sym_table.lookup_insert(token.name)
        elif isinstance(token, NumberToken):
            value = token.value
        elif isinstance(token, ExpressionNode):
            value = token
        return value

    def _stmt(self):
        var = self.tokens.next()
        var = self._value(var)
        assignment = self.tokens.next()
        self._expr()
        try:
            last = self.stack.pop()
            self.tree.add_node(assignment.op, var, self._value(last))
        except IndexError:
            raise SyntaxError("Unexpected end of expression")

    def _expr(self):
        for token in self.tokens:
            if isinstance(token, OperatorToken):
                try:
                    right = self.stack.pop()
                    left = self.stack.pop()
                except IndexError:
                    raise SyntaxError("Wrong number of operands")
                node = self.tree.add_node(token.op,
                                          self._value(left),
                                          self._value(right))
                self.stack.append(node)
            else:
                self.stack.append(token)

    def parse(self, stream):
        lineno = 0
        self.sym_table = SymbolTable()
        for line in lines(stream):
            lineno += 1
            self.stack = []
            lexer = Lexer(line)
            lexer.lineno = lineno
            self.tokens = lexer.tokens()
            self.tree = ExpressionTree()
            self._stmt()
            yield self.tree
