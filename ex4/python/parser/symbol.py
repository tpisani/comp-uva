
class Symbol(object):

    def __init__(self, name, id=None):
        self.id = id
        self.name = name

    def __str__(self):
        return "id{s.id}".format(s=self)

    def __repr__(self):
        return str(self)


class SymbolTable(object):

    def __init__(self):
        self.symbols = []

    def insert(self, name):
        sym = Symbol(name)
        if self.symbols:
            last = self.symbols[-1]
            sym.id = last.id + 1
        else:
            sym.id = 1
        self.symbols.append(sym)
        return sym

    def lookup(self, name):
        for sym in self.symbols:
            if sym.name == name:
                return sym
        return None

    def lookup_insert(self, name):
        sym = self.lookup(name)
        if not sym:
            sym = self.insert(name)
        return sym
