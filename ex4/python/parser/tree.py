from lexer.token import ADD, MINUS, TIMES, DIVIDE

from symbol import Symbol

INSTRUCTIONS = {
    ADD: "ADD",
    MINUS: "SUB",
    TIMES: "MUL",
    DIVIDE: "DIV",
}


class ExpressionNode(object):

    def __init__(self, op, left, right):
        self.op = op
        self.left = left
        self.right = right
        self.next = None
        self.temp = None
        self.reg = None

    def __str__(self):
        return "<{classname}: {e.left} {e.op} {e.right}>".format(classname=self.__class__.__name__,
                                                                 e=self)

    def __repr__(self):
        return str(self)

    def _eval(self, obj):
        if isinstance(obj, ExpressionNode):
            return obj.pseudocode()
        elif isinstance(obj, Symbol):
            return "id{0.id}".format(obj)
        return obj

    def _asm_eval(self, obj):
        if isinstance(obj, ExpressionNode):
            return obj.asm()
        elif isinstance(obj, Symbol):
            return "id{0.id}".format(obj)
        return obj

    def pseudocode(self, temp=0):
        if self.temp:
            return self.temp
        left = self._eval(self.left)
        right = self._eval(self.right)
        if self.op == "=":
            return "{left} = {result}".format(left=left,
                                              result=right)
        else:
            return "t{temp} = {left} {op} {right}".format(temp=temp,
                                                          left=left,
                                                          op=self.op,
                                                          right=right)

    def consume(self, temp=1):
        self.temp = "t{0}".format(temp)
        return self.temp

    def asm(self, reg=1):
        if self.reg:
            return self.reg
        left = self._asm_eval(self.left)
        right = self._asm_eval(self.right)
        if self.op == "=":
            return "STR {left}, {result}".format(left=left,
                                              result=right)
        else:
            op = INSTRUCTIONS.get(self.op)
            return "{op} R{reg} {left} {right}".format(op=op,
                                                       reg=reg,
                                                       left=left,
                                                       right=right)

    def allocate(self, reg=1):
        self.reg = "R{0}".format(reg)
        return self.reg

class ExpressionTree(object):

    def __init__(self):
        self.root = None
        self.last = None

    def __str__(self):
        return str(self.root)

    def __repr__(self):
        return str(self)

    def add_node(self, op, left, right):
        node = ExpressionNode(op, left, right)
        if self.root:
            self.root.next = node
        if not self.last:
            self.last = node
        self.root = node
        return node

    def traverse(self):
        current = self.last
        while current:
            yield current
            current = current.next

    def pseudocode(self):
        code = []
        i = 1
        for node in self.traverse():
            code.append(node.pseudocode(i))
            node.consume(i)
            i += 1
        return code

    def asm(self):
        code = []
        i = 1
        for node in self.traverse():
            code.append(node.asm(i))
            node.allocate(i)
            i += 1
        return code
