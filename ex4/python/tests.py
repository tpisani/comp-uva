import unittest

from lexer import Lexer
from lexer.token import *

from parser import Parser, PostfixParser
from parser.symbol import Symbol
from parser.tree import ExpressionNode


class LexerTests(unittest.TestCase):

    def test_should_ignore_whitespaces_and_EOL(self):
        lexer = Lexer("x  =  y - 2")
        for token in lexer.tokens():
            self.assertNotIn(token.string(), WHITESPACE)
            self.assertNotIn(token.string(), EOL)

    def test_should_raise_unrecognized_token_error_when_lexeme_is_not_within_rules(self):
        lexer = Lexer("x = y - n& + 2")
        try:
            lexer.validate()
            self.fail("UnrecognizedTokenError not raised")
        except UnrecognizedTokenError, e:
            if e.lineno != 1 or e.column != 10:
                self.fail("UnrecognizedTokenError was raised"
                          " with misleading lineno or column")

    def test_tokens_should_be_able_to_have_any_length(self):
        lexer = Lexer("x = n1 + number2")
        expected_tokens = (IdentifierToken("x"),
                           AssignmentOperator("="),
                           IdentifierToken("n1"),
                           AddOperator("+"),
                           IdentifierToken("number2"))
        i = -1
        for token in lexer.tokens():
            i += 1
            self.assertEqual(token, expected_tokens[i])


    def test_tokens_should_be_identified_without_whitespaces_between(self):
        lexer = Lexer("x = n1+2/5-z * w   -number2")
        expected_tokens = (IdentifierToken("x"),
                           AssignmentOperator("="),
                           IdentifierToken("n1"),
                           AddOperator("+"),
                           NumberToken("2"),
                           DivideOperator("/"),
                           NumberToken("5"),
                           MinusOperator("-"),
                           IdentifierToken("z"),
                           TimesOperator("*"),
                           IdentifierToken("w"),
                           MinusOperator("-"),
                           IdentifierToken("number2"))
        i = -1
        for token in lexer.tokens():
            i += 1
            self.assertEqual(token, expected_tokens[i])


class ParserTests(unittest.TestCase):

    def setUp(self):
        self.parser = Parser()

    def test_should_raise_syntax_error_for_invalid_syntax(self):
        try:
            self.parser.parse("x = 2 + 2 *").next()
            self.fail("SyntaxError not raised")
        except SyntaxError:
            pass

    def test_should_respect_operator_precedence(self):
        trees = self.parser.parse("x = 2 - 2 * 5 + 2")
        tree = trees.next()
        self.assertEqual(tree.root.op, "=")
        self.assertEqual(tree.last.op, "*")

    def test_should_be_able_to_parse_multiline_stream(self):
        trees = self.parser.parse("x = 2\ny = x + 5\n\nz = y - 10")
        x_tree = trees.next()
        self.assertEqual(x_tree.root.op, "=")
        y_tree = trees.next()
        self.assertEqual(y_tree.last.op, "+")
        z_tree = trees.next()
        self.assertEqual(z_tree.last.op, "-")


class PostfixParserTests(unittest.TestCase):

    def setUp(self):
        self.parser = PostfixParser()

    def test_should_raise_syntax_error_when_operands_are_not_sufficient(self):
        trees = self.parser.parse("x = 3 +")
        self.assertRaises(SyntaxError, trees.next)

    def test_should_process_operators_on_sight(self):
        trees = self.parser.parse("x = 100 5 8 - + 4 *")
        tree = trees.next()
        self.assertEqual(tree.root.op, "=")
        self.assertEqual(tree.last.op, "-")

    def test_should_be_able_to_parse_multiline_stream(self):
        trees = self.parser.parse("x = 10\n\ny = x 90 +")
        x_tree = trees.next()
        self.assertEqual(x_tree.root.op, "=")
        self.assertEqual(x_tree.root.right, 10)
        y_tree = trees.next()
        self.assertEqual(y_tree.last.op, "+")
        self.assertEqual(y_tree.last.right, 90)


class ExpressionTreeTests(unittest.TestCase):

    def test_node_should_generate_pseudocode_according_to_operator(self):
        x = Symbol("x", 1)
        y = Symbol("y", 2)

        node = ExpressionNode("+", 3, 5)
        self.assertEqual(node.pseudocode(), "t0 = 3 + 5")

        node = ExpressionNode("-", 40, 50)
        self.assertEqual(node.pseudocode(), "t0 = 40 - 50")

        node = ExpressionNode("*", x, 10)
        self.assertEqual(node.pseudocode(), "t0 = id1 * 10")

        node = ExpressionNode("=", x, 10)
        self.assertEqual(node.pseudocode(), "id1 = 10")

        node = ExpressionNode("/", x, y)
        self.assertEqual(node.pseudocode(), "t0 = id1 / id2")

        node = ExpressionNode("=", x, y)
        self.assertEqual(node.pseudocode(), "id1 = id2")

    def test_tree_should_replace_nodes_with_temp_vars(self):
        parser = PostfixParser()

        trees = parser.parse("x = 3 3 + 8 -")
        tree = trees.next()
        self.assertEqual(tree.pseudocode(), ["t0 = 3 + 3",
                                             "t1 = t0 - 8",
                                             "id1 = t1"])


if __name__ == "__main__":
    unittest.main()
