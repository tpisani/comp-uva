import argparse

from parser import Parser, PostfixParser


argparse = argparse.ArgumentParser()

argparse.add_argument("--infix", "-i",
                      action="store_true",
                      help="use infix notation")
argparse.add_argument("file")

arguments = argparse.parse_args()

if arguments.infix:
    parser = Parser()
else:
    parser = PostfixParser()


# Output pseudo code.

source = open(arguments.file, "r").read()
output = open("3addr.out", "w")
trees = parser.parse(source)
for tree in trees:
    pseudocode = tree.pseudocode()
    for line in pseudocode:
        output.write(line + "\n")
output.close()


# Output assembly code.

source = open(arguments.file, "r").read()
output = open("asm.out", "w")
trees = parser.parse(source)
for tree in trees:
    asm = tree.asm()
    for line in asm:
        output.write(line + "\n")
output.close()
