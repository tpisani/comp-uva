%{
    #include <stdio.h>
%}

%token EOL

%token NUMBER

%token PLUS

%token MINUS

%token TIMES

%token DIVIDE

%token IDENTIFIER


%left PLUS MINUS

%left TIMES DIVIDE

%%

line:
    | line input
;

input: EOL
    | expr EOL
;

expr: expr PLUS expr
    | expr MINUS expr
    | expr TIMES expr
    | expr DIVIDE expr
    | NUMBER
    | IDENTIFIER
;

%%

int yyerror(char *s) {
    printf("%s\n", s);
    return 1;
}

int main() {
    yyparse();
    return 0;
}
