%{
    #include "y.tab.h"
%}

%%

[0-9]+ {
    return NUMBER;
}

[a-zA-Z]+ {
    return IDENTIFIER;
}

"+" {
    return PLUS;
}

"-" {
    return MINUS;
}

"*" {
    return TIMES;
}

"/" {
    return DIVIDE;
}

";" {
    return EOL;
}

%%

int yywrap() {
    return 1;
}
