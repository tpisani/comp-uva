======================
UVA - Compilers 2014.2
======================

Repository to store the exercises and assignments of the course 'compilers' of Veiga de Almeida University.


Guide
-----

Each assignment is referenced by the format ``ex<number>``.
